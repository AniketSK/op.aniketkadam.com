---
title: Navigation Fragments Notes
date: "2019-01-06T04:44:07.521Z"
tags: ["navigation fragments","notes"]
---

# Notes on the navigation fragment
This will be about my notes on the Navigation Controller Architecture Component, about this [talk](https://www.youtube.com/watch?v=8GCXtCjtg40).

# Reasons to do it
* Adding navigation arch component can help you visualise the state of your app without much(?) effort and simplify your way of re-ordering screens.
Note: Passing data in your intents to the other activities
        Handling the onActivityFinish whatever it is

When should you do it?
* Onboarding new people
* Clarifying app flow to existing people.
    ** People, not engineers because your UI/UX and other teams will also benefit from having this kind of overview for the product
* When you want to do trunk based development
* Coming soon screens
* When you have rapid re-ordering often
* A/B testing simplified

# Notes:
Nav Host is the window where everything is replaced within.
Use by declaring a new fragment, the nav host fragment from the navigation library,  

READING YOUR WHOLE NAVIGATION GRAPH FROM A SERVER WAT?!
