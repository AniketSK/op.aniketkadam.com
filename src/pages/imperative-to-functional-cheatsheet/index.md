---
title: "Functional to Imperative Cheat Sheet"
date: "2019-06-20T13:37:31.269Z"
tags: ["functional","programming","imperative","cheat sheet"]
---

Here's how you convert Imperative -> Functional!

* if statements = filter
* two params, where one gives info to add things to the other = reduce
* one object transformed into another = map

Note: If you only care about one of the branches of the if condition, then use filter. Because you're not going to get access to the false case.

* if statements, when you care about both branches = flatMap
