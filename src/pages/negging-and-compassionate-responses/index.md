---
title: "Negging and Compassionate Responses"
date: "2019-05-03T03:36:18.843Z"
tags: ["compassion", "negging"]
---

Let's say that you hear "Oh you've got a great personal project but you should bring that motivation to your work also." how are you supposed to take such a statement?

It Depends.

We're going to take the case of two different people saying the above, to see how wildly that changes how you should respond.

- Case 1: from (an obnoxious) friend at a meetup.
- Case 2: An interviewer saying it to you.

## A Framework For Compassion

1. Think about why is _this_ person really saying it?
2. What are they really trying to say?
3. What do they stand to gain?

Now, it could've been a terrible insult and you could've spent the day feeling bad about it OR you could think about the three things above.

## Case 1: from (an obnoxious) friend at a meetup 

Your friend stands to gain nothing from putting you down, so perhaps it's just a broadcast of their own insecurity? Perhaps they truly get joy from their own projects and just haven't been able to bring that level of commitment to their (boring, illsuited) work?

#### Response
To such a friend, you may begin a conversation asking about how their work is, if they've been respected at work and how that's going. Then you might find out a comment from peers or a manager has lodged itself into their brain about this and you can help them find just what they need to get meaning from their work!

Because you didn't turn your gaze unnecessarily inward at a possible insult, you could help your friend!

## Case 2: An interviewer saying it to you.

What they want is pretty straightforward at this point. They want to hire you.

What? You may think. They're trying to lower your self confidence so that you agree to terms that favor them. But why attempt that unless they want to hire you? Who needs better terms from someone they don't ever intend to offer terms?

If you get negged, they want you.

Secondly, they have established themselves as a person willing to use psychological tricks to demean you into accepting a position, at possibly a lower pay. That's what negging is, to attack someone's self confidence so that they are more pliable (not nagging which is different).

#### Response
How you choose to respond to that information is upto you, you may choose to:
1. Break off the discussion and leave with a thanks.
2. To focus it on them with a "Is this lack of psychological safety part of the standard operating procedure at your company?"
3. Understand that they want you, be unaffected with their lies (they really like the project and that's why they switched to trying to hire you) and respond with something like "Of course! You're seeing what I'm like at work everyday :)"

Whichever way you choose to respond, an understanding of the situation will help you take the best outcome that you can have, without being affected by petty lies!

Think back on how this has been used in your daily life and how the power of compassionate understanding can inform how you work and hang out with friends!

[Share to Twitter](https://twitter.com/intent/tweet?&text=Here%27s%20how%20to%20compassionately%20respond%20to%20friends%20or%20interviewers%20putting%20your%20work%20down!&via=AniketSMK&tw_p=tweetbutton&url=https%3A%2F%2Fop.aniketkadam.com%2Fnegging-and-compassionate-responses%2F) • [Discuss on Twitter](https://twitter.com/search?q=https%3A%2F%2Fop.aniketkadam.com%2Fnegging-and-compassionate-responses%2F)