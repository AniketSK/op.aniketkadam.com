---
title: "Failure Log: Issue #1"
date: "2019-04-11T21:49:43.078Z"
tags: ["systems","failure","tech"]
---

Too much of tech looks like magic to those who haven't been steeped in it long enough. It can look like everyone is doing clean perfect work and when you inevitably run into bugs or roadblocks you wonder, are you not cut out for coding?

The (unintentional) secret is that seniors are running into bugs and roadblocks, even hours wasted due to typos, we just don't give it a second thought and accept it as part of the work.

So in the spirit of making that process a bit more open, I'm keeping a failure log. The things that didn't work today!

## Today's (temporary) fail: Adding Images to Gatsby Blogs
If you've seen my [previous blog](../building-systems-for-success) about Building Systems For Success, you'll see it has images.

Fun fact, I first wrote, deployed and tweeted it without adding the images. Getting a mimimum-viable-product style blog post out fast.

But then I went back and decided to add the images to it later. You'd think this was a simple process but it's unnecessarily hard in my opinion the first time you try it!

Here's what I did:

1. Googled: adding images to your gatsby blog post

This resulted in the gatsby docs [post](https://www.gatsbyjs.org/packages/gatsby-source-filesystem/) about images and [another](https://codebushi.com/using-gatsby-image/) one.

2. Trying to get the images to load

The images weren't loading and I was sure I'd messed up the paths. There's several things in there that didn't make sense right away and I couldn't see how I'd reference a file on disk in the markdown.
After creating images in the root images folder, in the pages folder and in yet more folders, the syntax and paths it's referring to worked and I deleted the rest and figured out the paths that work.

3. After installing the plugins and making some of the changes it's talking about I realised it's only to add images to the very top of your post. So back to the googling board to figure out how to add images in the markdown. I googled: adding images in the middle of a gatsby blog

4. That led me to [this](https://www.orangejellyfish.com/blog/a-comprehensive-guide-to-images-in-gatsby/) article. ****Which I skimmed**** I cannot stress this enough. If I sat reading entire articles I googled, this whole process would take ten times longer.

Somewhere in the middle it says "Images in Markdown" where I notice it wants me to install two more plugins, confiure them in the gatsby-config and finally I see something to add to the markdown.

I get distracted by it briefly skipping back to how to do single images in your layout and then get back to adding the actual image and now with a few more renaming issues, it works! Almost.

5. Now that everything else is done, I realise that at one point I needed three images side by side. Back to the googling to see how to do that. I google: arrange 4 images side by side gatsby-remark-images

From which I find this [github issue](https://github.com/gatsbyjs/gatsby/issues/3882) where they note that you basically...can't style them. Again, by skimming the page I find out this information in 20 seconds rather than several minutes.

6. Ok, so we can't style them, I don't want to put them one after the other, I'd rather not have them in there. What else can we do? Make them one image!

7. Round two, I decide to make the three images into one image, with just them appended side by side. This will give the illusion of having three images next to each other while still working with what I know.

I've previously done this sort of thing with [imagemagick](https://www.imagemagick.org/), the commandline image processing tool so I decide to reduce the size of all three images to a [third](http://www.imagemagick.org/Usage/resize/) of their original size, and how to [append them together](https://stackoverflow.com/questions/20737061/merge-images-side-by-sidehorizontally) both of which I have to google.

8. Finally we're done and it all works so I build and deploy :)


All this just so I can:
1. Have some images in the middle of my blog.
2. Have some of them side by side.

But since I've spent several years in software, I didn't hesitate or care that nothing was working for a good while, I just kept solving issues until it worked. It took me less time to do all that than to write up this blog post!

Keep working on things until they work, wasting zero time on self-doubt and you're senior as well.
