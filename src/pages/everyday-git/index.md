---
title: Everyday Git
date: "2018-12-16T04:44:07.521Z"
tags: ["git","workflow"]
---

# Philosophy of git:
1. Make small logical changes.
2. When it's working, go through your changes, reading once more for what you could do better.
3. Read *all* the diffs everytime, and selectively stage changes as you go.

# Make small logical changes:

1. Focused building
	You're only building exactly one feature at a time. Maximizing your ability. [Baddeley's model of working memory](https://www.youtube.com/watch?v=O96rPVgj_bI)

# When it's working, go through your changes, reading once more for what you could do better.

1. When you've gotten something working, hopefully you had to learn and strech the boundaries of what you know to achieve it. This usually means it's not the best code you've written.
2. Looking through the diffs, now that you've worked it out, lets you see the bigger picture and better abstractions that will help make this more maintainable.

# Read *all* the diffs everytime, and selectively stage changes as you go.
1. You will most likely find a bug. 
[moth](moth.jpg)
Every time I've commited thinking, yep I absolutely know what's in this, let's just commit and move on, I add a bug, or leave in something that shouldn't have been.



Here's what can easily get left behind if you don't read diffs and selectively stage.



Have suggestions for a topic I should cover? Send me a dm at [@AniketSMK](http://twitter.com/aniketsmk/) or email me at hello@\[firstname\]\[lastname\].com