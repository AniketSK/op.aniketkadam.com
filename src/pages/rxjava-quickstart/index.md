---
title: "RxJava Quickstart"
date: "2019-05-22T11:42:33.378Z"
tags: ["rxjava","quickstart","rxkotlin","map","reduce","guide"]
draft: true
---

Imagine the code you'd need to write on android to:
1. Validate some user data from a form
2. Save it into a local database
3. Check if the internet is available
4. Attempt to push it to the server
5. Show the user that the action succeded if it did.

Chances are, you're imagining a large usecase with many moving parts. How big would the presenter[?](https://antonioleiva.com/mvp-android/) for this be?

Let's see how we can reduce that down to a few words of code. That's right, words, not even lines.

For this, we'll be using some of most basic building blocks of functional programming, composed together:

## Observable Stream



## Map


## SwitchMap

## Flatmap



## Reduce



Have suggestions for a topic I should cover? Send me a dm at [@AniketSMK](http://twitter.com/aniketsmk/) or email me at hello@\[firstname\]\[lastname\].com