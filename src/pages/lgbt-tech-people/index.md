---
title: "LGBT Tech People"
date: "2019-06-02T08:03:16.148Z"
tags: ["lgbt","tech","community"]
---

###"Everyone deserves to live in the world, being true to who they are."

For LGBT Pride month, I thought it would be great to look into the current and past contributions to tech by LGBT people.

If you need a refresher on what the terms mean, here's a great [resource](https://www.glaad.org/files/allys-guide-to-terminology.pdf).


## The Future

The best way to get to know someone, is to read their own words. Here's a way to search for 🏳️‍🌈 tech people:

A lot of LGBT people put the rainbow flag in their twitter profile. They also tend to mention their programming language and framework of choice in it. So by searching for the flag and a framework/programming language, you could find all the ones who are open about their identities today!

Here's a quick start for javascript and Android folks.

[Javascript](https://twitter.com/search?q=%F0%9F%8F%B3%EF%B8%8F%E2%80%8D%F0%9F%8C%88%20javascript)

[Android](https://twitter.com/search?q=%F0%9F%8F%B3%EF%B8%8F%E2%80%8D%F0%9F%8C%88%20android)

---

## The Present
Limited by the people I've come across online, the ones I know are:

[Aditya Mukerjee](https://twitter.com/chimeracoder), risk engineer at Stripe, a wizard with Go. A very public advocate for civil rights and anti-racism. Bengali.

--

[Sophie Alpert](https://twitter.com/sophiebits/), who discovered React when it was a lone repository on GitHub, recognized its potential, contributed the most code to it in one of the early years and then brought it into Facebook with her advocacy. Check out the [podcast](https://sophiebits.com/2018/05/29/react-podcast-inside-react.html) she did.

--

[Coraline Ada](https://twitter.com/CoralineAda) created the [Contributor Covenant](http://contributor-covenant.org/) the most widely adopted Code of Conduct that has helped countless people feel safer in everything from conferences to contributing to code repositories. Notable rubyist. Read about her journey as a trans person [here](https://where.coraline.codes/).

--

[Radhika Morabia](https://twitter.com/radhikamorabia/) an amazing fun javascript dev and interactive [streamer](https://twitch.tv/rmorabia). She does fullstack engineering with React, Node and Python. Newsletter that lets you know the true story of tech.

--

[Keziyah](https://twitter.com/KeziyahL) Queer UX designer and digital nomad. Making the [Juniors In Tech](https://juniorsintech.com/) weekly newsletter and job board for juniors.

---

## The Past
[Alan Turing](https://en.wikipedia.org/wiki/Alan_Turing), known to all students of Computer Science for not creating only for the abstract mathematical model of computing called the [Turing Machine](https://en.wikipedia.org/wiki/Turing_machine) by which we gauge the completeness of programming languages today and forever, but also for saving hundreds of thousands of lives by enabling the mass breaking of German communication codes during World War 2.

The British government levied a deadly punishment on him just for being gay and he suffered for it. They wouldn't apologise for it until 57 years later, in [2009](https://www.theguardian.com/world/2009/sep/11/pm-apology-to-alan-turing).

--

[Christopher Strachey](https://en.wikipedia.org/wiki/Christopher_Strachey) pioneer in programming language design. Made a percursor to the C programming language.

--

[Edith Windsor](https://en.wikipedia.org/wiki/Edith_Windsor) a programmer and systems engineer at IBM who worked with UNIVAC. Brought a case in the United States that eventually resulted in the legalization of gay marriage with a supreme court verdict.

--
More sources [here](https://blog.newrelic.com/culture/10-lgbt-computer-science-pioneers/)

---

Have suggestions for a topic I should cover? Send me a dm at [@AniketSMK](http://twitter.com/aniketsmk/) or email me at hello@\[firstname\]\[lastname\].com