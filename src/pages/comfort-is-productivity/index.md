---
title: "Comfort is Productivity"
date: "2019-05-21T05:12:36.212Z"
tags: ["systems","non-technical","lifestyle", "productivity"]
---

## Consider a race!

Let's consider your job involves running, from point A to point B. The faster you do it, the better.

Lets say that for this job, you are running in sandles. Can you immediately see what the problem is?

It doesn't stop you from doing the job, sure you can still run. How much more effort are you putting in, to achieve lesser results though? What damage to your knees and other parts of your life have you put in because of your gear?

Let's take even a lesser example. Shoes that are a _touch_ too loose. Again you can run, it even feels almost comfortable. However you're still losing a bit of time and effort to something that's handicapping your performance for the entire duration of your job.

## Back to computers

How many minutes of your day are going into waiting for the computer to do something? How does that affect your state of mental flow and how does that affect your productivity? I guarantee you it's a snowball.
If you don't have an SSD and 12Gb of RAM, your boss doesn't understand what investment in employees means.

### Rock Climbing and other Activities
One really popular pastime among developers is rock climbing. Because it helps fight against the carpel tunnel you can get from typing a lot.

What if you made absolutely sure your chair, and desk and typing position were ideal? What if you invested in the right keyboard?

Get the perfect sized shoes. You'll save yourself time and effort that could be better applied to your work or your life.

Have suggestions for a topic I should cover? Send me a dm at [@AniketSMK](http://twitter.com/aniketsmk/) or email me at hello@\[firstname\]\[lastname\].com

[Share](https://twitter.com/intent/tweet?text=This%20article%20should%20inspire%20you%20to%20take%20better%20care%20of%20yourself%20while%20honing%20your%20skills!&via=AniketSMK&tw_p=tweetbutton&url=https%3A%2F%2Fop.aniketkadam.com%2Fcomfort-is-productivity) • [Discuss](https://twitter.com/search?q=op.aniketkadam.com%2Fcomfort-is-productivity)