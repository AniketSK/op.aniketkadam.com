---
title: "Competition doesn't scale"
date: "2019-05-25T15:17:28.604Z"
tags: ["systems","technical","lifestyle", "productivity"]
---

Think of all the really driven people you know. I'm sure all of us know a few.

Those who can best every odd and conquer any obstacle.

The winners of the competitive game. The few.

When you set up a competitive system, pit people against each other, you're going to drive people to succeed. You might be inclined to think the higher the stakes, the greater struggle and rewards.

However, there are two ways to win a competition.
1. Do better than everyone else.
2. Make sure that no one does better than you.


The easier option will simply be used more, so we end up with a whole lot of credit stealing, backstabbing and obstacle creating that results in overall lowered performance.

The unstoppable few survive, the rest are driven down far worse.
Competition's number one product is failure.


When push comes to shove, the backstabbing and distrust will increase and productivity drops.


What alternative do we have? What can scale to the size of teams?

## Collaboration does.

When you reward a team for overall performance and emphasize collaboration, you remove the ability to inflate individual perception via pulling people down.

By fostering a growth mindset and blamelessness, you maximize psychological safety and foster learning. This way, everyone grows communicates and performs as a cohesive unit.

During difficult times, they'd simply help each other and not lose performance. Your turnover would drop as well.

Compassion scales.


## Why do people and businesses so often choose competition?
I believe it's because of a three tiered of what to do. Here they are, ranked in effectiveness.

#### 1. Do nothing
By doing nothing, nothing is achieved.

A Lose - Lose situation.
#### 2. Use force.
This is always better than nothing, it's the primal driver that we're all born with. Rage, domination and competition are born from it.

A Win-Lose situation. For dominators vs dominated.
#### 3. The out of the box solution.
This requires thinking beyond immediate satisfaction and results. It prioritizes long term thinking and self-healing systems.

A Win-Win situation. For all those working together.


#### Epilogue
We are very quick to cling to the easiest thing that works, and it will undoubtedly become the most widespread first. We had to go from anarchy to monarchy to democracy. Each, increasing in complexity towards the greatest good.

It's time to take our businesses out their local maximas and strive for more.

-

I help businesses foster this dynamic through the entire organization, as an experienced engineer and [compassionate coder](http://compassionatecoding.com/). To help reform your org, reach out, and I'll get back to you once I'm done with my current client.

Have suggestions for a topic I should cover? Send me a dm at [@AniketSMK](http://twitter.com/aniketsmk/) or email me at hello (at) aniketkadam.com

[Share](https://twitter.com/intent/tweet?text=Competition%20doesn%27t%20scale.%20Find%20out%20what%20does.&via=AniketSMK&tw_p=tweetbutton&url=https%3A%2F%2Fop.aniketkadam.com%2Fcompetition-doesnt-scale) • [Discuss](https://twitter.com/search?q=op.aniketkadam.com%2Fcompetition-doesnt-scale)