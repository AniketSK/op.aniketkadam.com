---
title: Dagger2 Android
date: "2019-04-04T21:30:43.615Z"
tags: ["dagger-android"]
---

Let's go over a few terms and the simplest version of what they mean, before we get into how they're used.

##Component.
Defines the app injection point (the ones for activities are generated)

These are interfaces.

##Modules:
Each activity has its own module (can be shared)

These are concrete classes.

##Activity Builder:
Connect the modules to the activities they're part of.

These are abstract classes.


##Activity itself:
Must either extend DaggerAppCompatActivity or HasFragmentInjector, HasSupportFragmentInjector and the requisite functions (check out DaggerAppCompatActivity for an example)


## Scopes:
Scopes applied to one provider, must be applied to all those who touch anywhere that provider goes in the module itself, as well as the component it's associated with.

For instance, a network module involving a retrofit @Singleton, will require all its parts feeding into that retrofit instance to not only be singleton, but the *component that module is loaded into* to be singleton as well.

---
to be continued...


Have suggestions for a topic I should cover? Send me a dm at [@AniketSMK](http://twitter.com/aniketsmk/) or email me at hello@\[firstname\]\[lastname\].com