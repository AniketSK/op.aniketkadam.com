---
title: What is One Page (op)
date: "2018-09-26T21:26:23.000Z"
---

One Page is where I list the tiny information that made a concept click for me.

Want to learn about Dagger2 in 6 lines? This is the place to look!

Have suggestions for a topic I should cover? Send me a dm at [@AniketSMK](http://twitter.com/aniketsmk/) or email me at hello@\[firstname\]\[lastname\].com