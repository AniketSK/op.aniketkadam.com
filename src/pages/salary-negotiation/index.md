---
title: Salary Negotiation
date: "2019-01-06T14:58:09.389Z"
tags: ["negotiation"]
---

What do you think your salary is based on?

If you answered, the quality of your work, your effectiveness on the bottom line, your experience? You're wrong.

I would want to live in a world where that is true but I talk to a lot of engineers about salary and i've seen:
* New people being offered double of similarly experienced company veterans.
* I've been one of and seen other seniors paid less than the juniors they were managing.

###So what does your salary depend on?
Only Negotiation.

That's just it. If you want to spend a few hours learning how to do it, maybe a few days practising it, you could raise your compensation, your vacation, your time flexibility by several hundreds of percent.

Think you can't raise up your salary right now if there's 6 months left for your yearly review? [Read this](https://twitter.com/chiuki/status/992924718029131777).

Good base strategies are:
* [This small article](http://stephaniehurlburt.com/blog/2016/7/12/tips-for-negotiation), that shows you how to negotiate without being aggressive, but still being extremely effective by engineer and entrepreneur [Stephanie Hurlburt](https://twitter.com/sehurlburt/).
* [This essay](https://www.kalzumeus.com/2012/01/23/salary-negotiation/) about negotiation which was written by an engineer and entrepreneur [Patrick McKenzie](https://twitter.com/patio11/).

I wrote an [article](https://medium.com/@anikadamg/why-to-never-reveal-your-ctc-compensation-and-still-get-employed-7be7588236f3?source=personal_blog) on why you should never reveal your salary in a job interview.
If asked once say "I wouldn't like to talk about my previous CTC", if asked again say "I trust that you have the ability to consider my skills and your needs, and come to a fair number" and just keep on variations of that.

Good luck, and always talk salary with colleagues, never with companies.

Have suggestions for a topic I should cover? Send me a dm at [@AniketSMK](http://twitter.com/aniketsmk/) or email me at hello@[aniketkadam].com without the brackets.