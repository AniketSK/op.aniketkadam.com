---
title: Healthy Foods Made Fast
date: "2018-09-29T16:15:05.667Z"
tags: ["food","keto","recipes"]
---

Food Diaries!

# Main Courses!

A meal of three components:
1. Random vegetables, fried together
2. with some seasoning
3. and a main ingredient

Is an easy, fast way to get a healthy keto or even non-keto diet.

## Random Keto Vegetables
* Tomato
* Onion
* Capsicum
* Asparagus
* Broccoli
* Spinach

## Seasonings
* Green Chilis
* Chopped garlic
* Dry mango powder
* Pizza Mix
* Salt
* Pepper

## Main Ingredients (non-vegetarian)
* Chicken
* Eggs

## Main Ingredients (vegetarian)
* [Dangar](https://ruchirasonalkar.wordpress.com/2015/06/29/dangar-recipe/)
* Lentils
* Quinoa
* Soya
* Flax Egg
* Avocado
* Paneer

If you're vegetarian, you might need something to boost your protein like a low-carb whey (which is made from milk proteins) or Spirulina.
If you have additional concerns like uric acid please check before you begin taking these.



Have suggestions for a topic I should cover? Send me a dm at [@AniketSMK](http://twitter.com/aniketsmk/) or email me at hello@\[firstname\]\[lastname\].com