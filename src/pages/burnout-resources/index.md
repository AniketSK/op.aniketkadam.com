---
title: Burnout Resources
date: "2019-01-06T04:45:40.677Z"
tags: ["burnout"]
---

How to know if you're burned out? What do you feel when going into work everyday?
* Excited
* Neutral
* Dread

If you picked option 3, you've definitely got burnout. Thinking about why exactly you feel it, and what you're dreading will help you hone in on what is the toxic element in your office.
Note: If you feel it's you, try to think about who gave you the idea to think that way, see what they got from you blaming yourself.

Do you wake up in the middle of the night, terrified about a bug and suddenly knowing where it is and how to fix it? That's a panic attack.

**It's not your fault.**

Psychological manipulation has put into a position where you're driven beyond your breaking point.
What's the turnover rate for your company? If it's high, it's very likely that's just how they operate and how they think they should run businesses. You don't deserve that life, they don't deserve your loyalty.

[It's ok to leave uncomfortable situations](https://twitter.com/ErynnBrook/status/1046055387617775616)

**Your work ethic can only survive in organizations that value YOU as much as you value your work.**

##Resources that will help you understand what's happened to you.

Have a look at what toxic environments can do and encouragement for getting out them.
[It's NOT like this everywhere](https://twitter.com/sehurlburt/status/942237972975382528)

Technique zero - taking down someone's self esteem to make them more pliable to orders
https://xkcd.com/1027/

Understand what's being done to you by reading [Sick Systems](http://www.issendai.com/psychology/sick-systems.html)

[Warning Signs of Abusive Companies and People](https://stephaniehurlburt.com/blog/2017/6/15/warning-signs-of-abusive-companies-and-people)

More subtle, everyday mechanics of toxic systems.
[Asking questions can ruin someone's life](https://stephaniehurlburt.com/blog/2017/10/17/danger-of-questions)

Please get yourself a therapist that you can trust, you might need to go through a few bad ones to find one that works for you. If you're in Mumbai, I can give you recommendations. Otherwise, I'm sorry and please keep trying.

##What could you do right away?
Chances are, you missing a deadline isn't going to kill anyone. You trying to make an impossible deadline might end up permanently hurting your body.

One step you could take is to establish and enforce your boundaries, come in on time and leave on time no matter what. Don't break this even if you start to feel better.


Need help? Send me a dm at [@AniketSMK](http://twitter.com/aniketsmk/) or email me at hello@[aniketkadam].com without the brackets.