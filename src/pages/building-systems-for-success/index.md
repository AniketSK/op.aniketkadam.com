---
title: "Building Systems: Applying casino rules to your life"
date: "2019-04-11T19:24:39.241Z"
tags: ["systems","non-technical","lifestyle"]
---

## What is a system exactly?

It's about making the good actions easy and the bad ones hard.

They're a way to support and scale behaviours. Whether it's monthly subscription (a system built to make it easy to give someone your money) or attendance tracking (a system made to keep people feeling watched), they achieve their goals. Systems are powerful.

The most well known is probably a casino and it sums up the ethos. The house always wins. The odds do result in the odd person winning a jackpot but that doesn't matter. The casino is guaranteed to be the winner in the long run.

Maybe not all the time, some people will always cancel before that free trial ends, or find innovative methods to cheat the attendance and there will always be jackpot winners but none of that matters over time.

## Setting yourself up for success

Let's take a case study, let's say you want to spend less time on twitter and more time reading.

What ways can you think of to make twitter hard and reading books easy?

### Make the bad thing hard
In Android 9 (pie) you can set app timers, the maximum time you want to do something before it's locked till the next day. 

Doing this makes opening twitter a 4 click process instead of a 1 one click, because you have to go disable the timer.

What that does, is break the instinctual opening of twitter.

![three screenshots showing various clicks that are required to start twitter after it's blocked by the android wellness app timer feature](./images/twitter-blocked.png)

### Make the good thing easy
This is more straightforward, just set a home screen shortcut for books that you want to read. Or stow your phone away in the storage area and keeps books by your bed.

![mobile screenshot showing two books Besharam by Priyanka Alika-Elias and Bad Blood by John Carreyrou](./images/home-books.png)

This way, you might go to open twitter but when you get bounced off, you remeber that you want to read a book instead and open that. Over time, you will retrain yourself to prefer books.

### Small steps add up to huge gains
Again, it doesn't matter that you still open twitter sometimes. In the long run, you're now reading far more and using twitter far less.

### Share your ways!
What ways can you think of to make the goals you want to achieve easier to work towards and the distractions that take you away from it, harder?

Have suggestions for a topic I should cover? Send me a dm at [@AniketSMK](http://twitter.com/aniketsmk/) or email me at hello@\[firstname\]\[lastname\].com